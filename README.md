## 功能简介

### 需要 HBase 服务器开放的端口号
2181
16000
16020

### 连接参数

```shell
hbase.zookeeper.quorum=192.168.72.136
hbase.zookeeper.property.clientPort=2181
hbase.master.port=16000
hbase.regionserver.port=16020
```

### 运行方式

将项目打包成 Jar 包，使用 java 命令运行 Jar 包

```shell
java -jar hbasecli-0.1.0.jar --server.port=8000 --hbase.zookeeper.quorum=192.168.72.136  --hbase.zookeeper.property.clientPort=2181 --hbase.master.port=16000 --hbase.regionserver.port=16020
```

### 浏览器访问 http://127.0.0.1:8000/

## 项目更新

### 版本 0.1.0

 - 新特性：后端解析

 > （1）HBaseCli 从数据库获取到字节 ( byte[] ) 形式的数据并进行解析，序列化为 JSON 传递给浏览器完成渲染
 >
 > （2）支持多种数据类型：字符串 ( UTF-8 ) 、Int、Long、Float、Double、Boolean 等
 >
 > （3）可利用 Java 和 Spring 的 SPI 方式注册自定义的数据类型

 - 新特性：前端解析

 > （1）HBaseCli 从数据库获取到字节 ( byte[] ) 形式的数据，使用 Protobuf 完成序列化进行网络传输，由浏览器完成二进制数据的解析工作
 > 
 > （2）支持多种数据类型：字符串 ( UTF-8 ) 、Int、Long、Float、Double、Boolean 等

 - 新特性：切换主题

 > （1）优化默认的 UI 样式
 >
 > （2）增加暗色 UI 样式

 - 更新：支持多个连接

 > （1）HBaseCli 同时维护多个连接，可以在 Web UI 中创建新的连接
 >
 > （2）默认情况下，HBaseCli 不会在启动时立刻建立连接
 >
 > （3）通过配置 `hbasecli.immediate=true` 可以让 HBaseCli 在启动时立刻建立连接

 - 更新：调整数据类型的命名

| 版本 0.0.1  | 版本 0.1.0  |
|-----------|-----------|
| `string`  | `UTF-8`   |
| `boolean` | `Boolean` |
| `short`   | `Short`   |
| `integer` | `Int`     |
| `long`    | `Long`    |
| `float`   | `Float`   |
| `double`  | `Double`  |

![](./front-end/src/assets/image/image1.png)

![](./front-end/src/assets/image/image2.png)

![](./front-end/src/assets/image/image3.png)

![](./front-end/src/assets/image/image4.png)

![](./front-end/src/assets/image/image5.png)

![](./front-end/src/assets/image/image6.png)

### 版本 0.0.1

![](./front-end/src/assets/image/screenshot.png) 


