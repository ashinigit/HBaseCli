const fs = require('fs')
const path = require('path')

const mainJsPath = path.resolve(__dirname, 'src/main.js')
fs.readFile(mainJsPath, 'utf-8', (err, data) => {
  if(err) {
    console.error('Error read main.js')
    return
  }
  const content = data.replace(/import\s+'\.\/mock'\n?/g, '// import \'./mock\'\n')
  fs.writeFile(mainJsPath, content, 'utf-8', (err) => {
    if(err) {
      console.error('Error write main.js')
    } else {
      console.log('Successfully removed import \'./mock\'')
    }
  })
})