module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    // 可选链插件 ?.
    '@babel/plugin-transform-optional-chaining',
    // 空值合并运算符 ??
    '@babel/plugin-transform-nullish-coalescing-operator',
    // 逻辑空值赋值运算符 ??=
    '@babel/plugin-transform-logical-assignment-operators'
  ]
}
