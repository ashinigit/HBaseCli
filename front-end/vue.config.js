const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true, // 转译依赖
  publicPath: './', // './'指使用相对路径打包
  outputDir: '../src/main/resources/static',
  productionSourceMap: process.env.NODE_ENV === 'development', //减小生产环境下的体积
  devServer: {
    port: 8081,
    open: false, // 启动后是否自动打开网页
    proxy: {
      // 配置开发服务器的后台地址
      // 参考文档： https://cli.vuejs.org/zh/config/#devserver-proxy
      // 处理接口的跨域问题
      '/api': {
        // /api/* 格式的接口默认发送同源请求 http://localhost:8081/api/*
        // 经过下面的配置后自动转入 http://127.0.0.1:8000/api/*
        target: 'http://127.0.0.1:8000/api',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
})
