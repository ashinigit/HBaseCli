import dayjs from 'dayjs'
import Mock from 'mockjs'
import qs from 'qs'

const baseURL = '/mock-api'

const timestampString = dateTime => (dayjs(dateTime, 'YYYY-MM-DD HH:mm:ss').unix() * 1000).toString()

const success = data => ({ code: 0, message: 'success', data })

Mock.mock(RegExp(`${baseURL}/defaultConnectionParams.*`), 'get', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success([])
})

Mock.mock(`${baseURL}/connections`, 'post', request => {
  const params = JSON.parse(request.body)
  console.log('请求', request, '参数', params)
  return success({
    id: '1',
    name: '127.0.0.1',
    params: {
      param1: 'param1',
      param2: 'param2',
      param3: 'param3'
    },
    running: true,
    createTime: timestampString('2000-01-01 08:30:00')
  })
})

Mock.mock(`${baseURL}/connections`, 'get', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success([
    {
      id: '1',
      name: '127.0.0.1',
      params: {
        param1: 'param1',
        param2: 'param2',
        param3: 'param3'
      },
      running: true,
      createTime: timestampString('2000-01-01 10:30:00')
    },
    {
      id: '2',
      name: '127.0.0.2',
      params: {
        param1: 'param1',
        param2: 'param2',
        param3: 'param3'
      },
      running: true,
      createTime: timestampString('2000-01-01 09:30:00')
    },
    {
      id: '3',
      name: '127.0.0.3',
      params: {
        param1: 'param1',
        param2: 'param2',
        param3: 'param3'
      },
      running: true,
      createTime: timestampString('2000-01-01 08:30:00')
    }
  ])
})

Mock.mock(RegExp(`${baseURL}/connections.*`), 'get', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success({
    id: params['id'],
    name: '127.0.0.1',
    params: {
      param1: 'param1',
      param2: 'param2',
      param3: 'param3'
    },
    running: true,
    createTime: timestampString('2000-01-01 08:30:00')
  })
})

Mock.mock(RegExp(`${baseURL}/connections.*`), 'delete', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success({
    id: params['id'],
    name: '127.0.0.1',
    params: {
      param1: 'param1',
      param2: 'param2',
      param3: 'param3'
    },
    running: false,
    createTime: timestampString('2000-01-01 08:30:00')
  })
})

Mock.mock(RegExp(`${baseURL}/tables.*`), 'get', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success([
    {
      name: 'table1',
      families: [
        {
          name: 'family1',
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family2', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family3', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        }
      ]
    },
    {
      name: 'table2',
      families: [
        {
          name: 'family1', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family2', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family3', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        }
      ]
    },
    {
      name: 'table3',
      families: [
        {
          name: 'family1', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family2', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        },
        {
          name: 'family3', 
          info: {
            param1: 'param1',
            param2: 'param2',
            param3: 'param3'
          }
        }
      ]
    }
  ])
})

Mock.mock(`${baseURL}/tables`, 'post', request => {
  const params = JSON.parse(request.body)
  console.log('请求', request, '参数', params)
  return success(true)
})

Mock.mock(RegExp(`${baseURL}/tables.*`), 'delete', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success(true)
})

Mock.mock(RegExp(`${baseURL}/dataTypes.*`), 'get', request => {
  const params = qs.parse(request.url.split('?')[1])
  console.log('请求', request, '参数', params)
  return success([
    'Float',
    'UTF-8',
    'Long',
    'Hex',
    'Boolean',
    'Double',
    'Short',
    'Int',
    'BigDecimal'
  ])
})

Mock.mock(`${baseURL}/row/query`, 'post', request => {
  const params = JSON.parse(request.body)
  console.log('请求', request, '参数', params)
  return success([
    {
      rowKey: 'rowKey1',
      cells: [
        {
          family: 'family1',
          qualifier: 'qualifier1',
          value: 'value1'
        },
        {
          family: 'family1',
          qualifier: 'qualifier2',
          value: 'value2'
        },
        {
          family: 'family1',
          qualifier: 'qualifier3',
          value: 'value3'
        }
      ]
    },
    {
      rowKey: 'rowKey2',
      cells: [
        {
          family: 'family2',
          qualifier: 'qualifier1',
          value: 'value1'
        },
        {
          family: 'family2',
          qualifier: 'qualifier2',
          value: 'value2'
        },
        {
          family: 'family3',
          qualifier: 'qualifier3',
          value: 'value3'
        }
      ]
    },
    {
      rowKey: 'rowKey3',
      cells: [
        {
          family: 'family3',
          qualifier: 'qualifier1',
          value: 'value1'
        },
        {
          family: 'family3',
          qualifier: 'qualifier2',
          value: 'value2'
        },
        {
          family: 'family3',
          qualifier: 'qualifier3',
          value: 'value3'
        }
      ]
    }
  ])
})

Mock.mock(`${baseURL}/row/create`, 'post', request => {
  const params = JSON.parse(request.body)
  console.log('请求', request, '参数', params)
  return success(true)
})

Mock.mock(`${baseURL}/row/delete`, 'post', request => {
  const params = JSON.parse(request.body)
  console.log('请求', request, '参数', params)
  return success(true)
})

export default Mock