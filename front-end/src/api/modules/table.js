import { get, postJson, del } from '../http'

/**
 * 
 * @param {string} connectionId 
 * @returns 
 */
function listTable(connectionId) {
  return get('/tables', { connectionId })
}

/**
 * 
 * @param {string} connectionId 
 * @param {string} tableName 
 * @param {string[]} families 
 * @returns 
 */
function createTable(connectionId, tableName, families) {
  return postJson('/tables', {
    connectionId,
    tableName,
    families,
    deleteIfExists: false
  })
}

/**
 * 
 * @param {string} connectionId 
 * @param {string[]} tables  
 * @returns
 */
function deleteTable(connectionId, tables) {
  return del('/tables', {
    connectionId,
    name: tables.join(',')
  })
}

export default { listTable, createTable, deleteTable }
