import { get, postJson, del } from '../http'

function getDefaultConnectionParams() {
  return get('/defaultConnectionParams')
}

/**
 * 
 * @param {{name: string, value: string}[]} params 
 * @returns 
 */
function createConnection(params) {
  return postJson('/connections', params)
}

function getAllConnection() {
  return get('/connections')
}

/**
 * 
 * @param {string} id 
 * @returns 
 */
function getConnectionById(id) {
  return get(`/connections/${id}`)
}

/**
 * 
 * @param {string} id 
 * @returns 
 */
function deleteConnectionById(id) {
  return del(`/connections/${id}`)
}

export default { 
  getDefaultConnectionParams, 
  createConnection, 
  getAllConnection, 
  getConnectionById,
  deleteConnectionById
}