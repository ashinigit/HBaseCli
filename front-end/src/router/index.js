import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: () => import('../views/MainPage.vue'),
    redirect: 'connection', // 默认页面
    children: [
      {
        path: 'connection',
        name: 'Connection',
        meta: {
          title: '连接',
          keepAlive: true
        },
        component: () => import('../views/main-page/Connection.vue')
      },
      {
        path: 'query',
        name: 'Query',
        meta: {
          title: '查询',
          keepAlive: true
        },
        component: () => import('../views/main-page/Query.vue')
      },
      {
        path: 'help',
        name: 'Help',
        meta: {
          title: '帮助',
          keepAlive: false
        },
        component: () => import('../views/main-page/Help.vue')
      }
    ]
  },
  {
    path: '/404',
    name: 'NotFound',
    meta: {
      title: '找不到页面',
      keepAlive: false
    },
    component: () => import('../views/error-page/NotFound.vue')
  },
  {
    path: '/:pathMatch(.*)',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

NProgress.configure({
  showSpinner: false
})

router.beforeEach((to, _, next) => {
  let title = ''
  if (to && to.meta && to.meta.title) {
    title = to.meta.title
  }
  NProgress.start()
  document.title = title
  next()
})

router.afterEach(() => {
  NProgress.done()
})

export default router
