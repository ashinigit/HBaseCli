const state = {
  defaultParams: [
    {
      name: 'hbase.zookeeper.quorum',
      value: ''
    },
    {
      name: 'hbase.zookeeper.property.clientPort',
      value: ''
    },
    {
      name: 'hbase.master.port',
      value: ''
    },
    {
      name: 'hbase.regionserver.port',
      value: ''
    }
  ],
  connections: []
}
const getters = {
  getDefaultParams: state => state.defaultParams,
  getConnections: state => state.connections
}
const mutations = {
  setDefaultParams: (state, newVal) => {
    state.defaultParams = newVal
  },
  setConnections: (state, newVal) => {
    state.connections = newVal
  }
}
const actions = {
  setDefaultParams: (context, newVal) => {
    context.commit('setDefaultParams', newVal)
  },
  setConnections: (context, newVal) => {
    context.commit('setConnections', newVal)
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
