import Vue from 'vue'
import Vuex from 'vuex'
import moduleProfile from './modules/profile'
import moduleGlobal from './modules/global'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    profile: moduleProfile,
    global: moduleGlobal
  },
  plugins: []
})
