import Vue from 'vue'

// 导入element-ui,语言设置为中文
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/zh-CN'
Vue.use(ElementUI, { locale })

// 导入element-ui的样式
import 'element-ui/lib/theme-chalk/index.css'

// 导入vue-router
import router from './router'

// 导入vuex
import store from './store'
Vue.use(store)
Vue.prototype.$store = store

// 导入axios
import axios from './api/http'
Vue.prototype.$http = axios

// 导入dayjs
import dayjs from 'dayjs'
import * as isLeapYear from 'dayjs/plugin/isLeapYear'
import 'dayjs/locale/zh-cn'
dayjs.extend(isLeapYear)
dayjs.locale('zh-cn')

// 导入 vue-json-viewer
import JsonViewer from 'vue-json-viewer'
Vue.use(JsonViewer)

// 导入全局样式
import './assets/scss/index.scss'

// 注册全局组件
import EChart from './components/EChart.vue'
Vue.component('e-chart', EChart)
import TagInput from './components/TagInput.vue'
Vue.component('tag-input', TagInput)

Vue.config.productionTip = false

// 使用 mockjs 模拟接口, 生产环境下要去掉这条import语句
// // // // // // // // // // import './mock'


// 挂载App
import App from './App.vue'
new Vue({ render: (h) => h(App), router }).$mount('#app')
