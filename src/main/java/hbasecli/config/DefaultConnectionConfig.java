package hbasecli.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DefaultConnectionConfig {

    @Value("${hbase.zookeeper.quorum}")
    private String zookeeperQuorum;

    @Value("${hbase.zookeeper.property.clientPort:2181}")
    private String zookeeperPropertyClientPort;

    @Value("${hbase.master.port:16000}")
    private String masterPort;

    @Value("${hbase.regionserver.port:16020}")
    private String regionServerPort;

    @Value("${hbasecli.immediate}")
    private String immediate;

    public String getZookeeperQuorum() {
        return zookeeperQuorum;
    }

    public String getZookeeperPropertyClientPort() {
        return zookeeperPropertyClientPort;
    }

    public String getMasterPort() {
        return masterPort;
    }

    public String getRegionServerPort() {
        return regionServerPort;
    }

    public String getImmediate() {
        return immediate;
    }

    @Override
    public String toString() {
        return "DefaultConnectionConfig{" +
                "zookeeperQuorum='" + zookeeperQuorum + '\'' +
                ", zookeeperPropertyClientPort='" + zookeeperPropertyClientPort + '\'' +
                ", masterPort='" + masterPort + '\'' +
                ", regionServerPort='" + regionServerPort + '\'' +
                ", immediate='" + immediate + '\'' +
                '}';
    }

}