package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class FloatDataType implements DataType {

    @Override
    public String name() {
        return "Float";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Float.toString(Bytes.toFloat(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Float.parseFloat(string));
    }

}
