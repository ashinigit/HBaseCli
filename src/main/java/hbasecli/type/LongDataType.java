package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class LongDataType implements DataType {

    @Override
    public String name() {
        return "Long";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Long.toString(Bytes.toLong(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Long.parseLong(string));
    }

}
