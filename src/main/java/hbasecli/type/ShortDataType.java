package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class ShortDataType implements DataType {

    @Override
    public String name() {
        return "Short";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Short.toString(Bytes.toShort(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Short.parseShort(string));
    }

}
