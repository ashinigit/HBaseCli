package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class BooleanDataType implements DataType {

    @Override
    public String name() {
        return "Boolean";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Boolean.toString(Bytes.toBoolean(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Boolean.parseBoolean(string));
    }

}
