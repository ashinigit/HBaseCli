package hbasecli.type;

public interface DataType {

    default String name() {
        return this.getClass().getName();
    }

    String bytesToString(byte[] bytes);

    byte[] stringToBytes(String string);

}
