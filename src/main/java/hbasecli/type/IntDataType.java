package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class IntDataType implements DataType {

    @Override
    public String name() {
        return "Int";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Integer.toString(Bytes.toInt(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Integer.parseInt(string));
    }

}
