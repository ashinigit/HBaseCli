package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class HexDataType implements DataType {

    @Override
    public String name() {
        return "Hex";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Bytes.toHex(bytes);
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.fromHex(string);
    }

}
