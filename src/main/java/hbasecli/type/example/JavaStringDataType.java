package hbasecli.type.example;

import hbasecli.type.DataType;
import org.apache.hadoop.hbase.util.Bytes;

@Deprecated
public class JavaStringDataType implements DataType {

    @Override
    public String name() {
        return "JavaString";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Bytes.toStringBinary(bytes);
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytesBinary(string);
    }

}
