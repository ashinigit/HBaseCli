package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class DoubleDataType implements DataType {

    @Override
    public String name() {
        return "Double";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Double.toString(Bytes.toDouble(bytes));
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(Double.parseDouble(string));
    }

}
