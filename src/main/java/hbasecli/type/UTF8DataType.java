package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

public class UTF8DataType implements DataType {

    @Override
    public String name() {
        return "UTF-8";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Bytes.toString(bytes);
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(string);
    }

}
