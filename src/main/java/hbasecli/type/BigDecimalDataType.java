package hbasecli.type;

import org.apache.hadoop.hbase.util.Bytes;

import java.math.BigDecimal;

public class BigDecimalDataType implements DataType {

    @Override
    public String name() {
        return "BigDecimal";
    }

    @Override
    public String bytesToString(byte[] bytes) {
        return Bytes.toBigDecimal(bytes).toString();
    }

    @Override
    public byte[] stringToBytes(String string) {
        return Bytes.toBytes(new BigDecimal(string));
    }

}
