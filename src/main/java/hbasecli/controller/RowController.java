package hbasecli.controller;

import java.util.List;
import javax.annotation.Resource;

import hbasecli.model.json.param.CreateRowParam;
import hbasecli.model.json.param.DeleteRowParam;
import hbasecli.model.json.param.QueryParam;
import hbasecli.model.json.JsonResponse;
import hbasecli.model.json.RowObject;
import hbasecli.service.HBaseServiceForJson;
import org.springframework.web.bind.annotation.*;

@RestController
public class RowController {

    @Resource
    private HBaseServiceForJson service;

    @PostMapping("/api/row/query")
    public JsonResponse<List<RowObject>> query(@RequestBody QueryParam param) {
        return service.query(param);
    }

    @PostMapping("/api/row/create")
    public JsonResponse<Boolean> createRow(@RequestBody CreateRowParam param) {
        return service.createRow(param);
    }

    @PostMapping("/api/row/delete")
    public JsonResponse<Boolean> deleteRow(@RequestBody DeleteRowParam param) {
        return service.deleteRow(param);
    }

}
