package hbasecli.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    // http://localhost:8000/
    @GetMapping({"/", "/index"})
    public String toIndex() {
        return "index.html";
    }

}
