package hbasecli.controller;

import javax.annotation.Resource;
import java.util.List;

import hbasecli.model.json.JsonResponse;
import hbasecli.type.DataTypes;
import org.springframework.web.bind.annotation.*;

import static hbasecli.model.json.JsonResponse.success;

@RestController
public class DataTypeController {

    @Resource
    private DataTypes dataTypes;

    @GetMapping("/api/dataTypes")
    JsonResponse<List<String>> getDataTypes() {
        return success(dataTypes.list());
    }

}
