package hbasecli.exception;

public class HBaseDataAccessException extends RuntimeException {
    private final String msg;

    public HBaseDataAccessException(String msg) {
        super();
        this.msg = msg;
    }

    public HBaseDataAccessException(Throwable throwable) {
        super();
        if (throwable != null) {
            this.msg = throwable.getMessage();
            if (throwable != this) {
                addSuppressed(throwable);
            }
        } else {
            this.msg = "未知的错误";
        }
    }

    public String getMsg() {
        return msg;
    }

}