package hbasecli.exception;

public class ParamException extends RuntimeException {
    private final String msg;

    public ParamException(String msg) {
        super();
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

}
