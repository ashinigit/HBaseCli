// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Protos.proto

// Protobuf Java Version: 3.25.3
package hbasecli.binary.model;

/**
 * Protobuf type {@code CellParam}
 */
public final class CellParam extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:CellParam)
    CellParamOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CellParam.newBuilder() to construct.
  private CellParam(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CellParam() {
    family_ = com.google.protobuf.ByteString.EMPTY;
    qualifier_ = com.google.protobuf.ByteString.EMPTY;
    value_ = com.google.protobuf.ByteString.EMPTY;
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new CellParam();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return hbasecli.binary.model.Protos.internal_static_CellParam_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return hbasecli.binary.model.Protos.internal_static_CellParam_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            hbasecli.binary.model.CellParam.class, hbasecli.binary.model.CellParam.Builder.class);
  }

  public static final int FAMILY_FIELD_NUMBER = 1;
  private com.google.protobuf.ByteString family_ = com.google.protobuf.ByteString.EMPTY;
  /**
   * <code>bytes family = 1;</code>
   * @return The family.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString getFamily() {
    return family_;
  }

  public static final int QUALIFIER_FIELD_NUMBER = 2;
  private com.google.protobuf.ByteString qualifier_ = com.google.protobuf.ByteString.EMPTY;
  /**
   * <code>bytes qualifier = 2;</code>
   * @return The qualifier.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString getQualifier() {
    return qualifier_;
  }

  public static final int VALUE_FIELD_NUMBER = 3;
  private com.google.protobuf.ByteString value_ = com.google.protobuf.ByteString.EMPTY;
  /**
   * <code>bytes value = 3;</code>
   * @return The value.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString getValue() {
    return value_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!family_.isEmpty()) {
      output.writeBytes(1, family_);
    }
    if (!qualifier_.isEmpty()) {
      output.writeBytes(2, qualifier_);
    }
    if (!value_.isEmpty()) {
      output.writeBytes(3, value_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!family_.isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(1, family_);
    }
    if (!qualifier_.isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(2, qualifier_);
    }
    if (!value_.isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(3, value_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof hbasecli.binary.model.CellParam)) {
      return super.equals(obj);
    }
    hbasecli.binary.model.CellParam other = (hbasecli.binary.model.CellParam) obj;

    if (!getFamily()
        .equals(other.getFamily())) return false;
    if (!getQualifier()
        .equals(other.getQualifier())) return false;
    if (!getValue()
        .equals(other.getValue())) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + FAMILY_FIELD_NUMBER;
    hash = (53 * hash) + getFamily().hashCode();
    hash = (37 * hash) + QUALIFIER_FIELD_NUMBER;
    hash = (53 * hash) + getQualifier().hashCode();
    hash = (37 * hash) + VALUE_FIELD_NUMBER;
    hash = (53 * hash) + getValue().hashCode();
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static hbasecli.binary.model.CellParam parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static hbasecli.binary.model.CellParam parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static hbasecli.binary.model.CellParam parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public static hbasecli.binary.model.CellParam parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }

  public static hbasecli.binary.model.CellParam parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static hbasecli.binary.model.CellParam parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(hbasecli.binary.model.CellParam prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code CellParam}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:CellParam)
      hbasecli.binary.model.CellParamOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return hbasecli.binary.model.Protos.internal_static_CellParam_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return hbasecli.binary.model.Protos.internal_static_CellParam_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              hbasecli.binary.model.CellParam.class, hbasecli.binary.model.CellParam.Builder.class);
    }

    // Construct using hbasecli.binary.model.CellParam.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      family_ = com.google.protobuf.ByteString.EMPTY;
      qualifier_ = com.google.protobuf.ByteString.EMPTY;
      value_ = com.google.protobuf.ByteString.EMPTY;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return hbasecli.binary.model.Protos.internal_static_CellParam_descriptor;
    }

    @java.lang.Override
    public hbasecli.binary.model.CellParam getDefaultInstanceForType() {
      return hbasecli.binary.model.CellParam.getDefaultInstance();
    }

    @java.lang.Override
    public hbasecli.binary.model.CellParam build() {
      hbasecli.binary.model.CellParam result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public hbasecli.binary.model.CellParam buildPartial() {
      hbasecli.binary.model.CellParam result = new hbasecli.binary.model.CellParam(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(hbasecli.binary.model.CellParam result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.family_ = family_;
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.qualifier_ = qualifier_;
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.value_ = value_;
      }
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof hbasecli.binary.model.CellParam) {
        return mergeFrom((hbasecli.binary.model.CellParam)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(hbasecli.binary.model.CellParam other) {
      if (other == hbasecli.binary.model.CellParam.getDefaultInstance()) return this;
      if (other.getFamily() != com.google.protobuf.ByteString.EMPTY) {
        setFamily(other.getFamily());
      }
      if (other.getQualifier() != com.google.protobuf.ByteString.EMPTY) {
        setQualifier(other.getQualifier());
      }
      if (other.getValue() != com.google.protobuf.ByteString.EMPTY) {
        setValue(other.getValue());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              family_ = input.readBytes();
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              qualifier_ = input.readBytes();
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              value_ = input.readBytes();
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private com.google.protobuf.ByteString family_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <code>bytes family = 1;</code>
     * @return The family.
     */
    @java.lang.Override
    public com.google.protobuf.ByteString getFamily() {
      return family_;
    }
    /**
     * <code>bytes family = 1;</code>
     * @param value The family to set.
     * @return This builder for chaining.
     */
    public Builder setFamily(com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      family_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <code>bytes family = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearFamily() {
      bitField0_ = (bitField0_ & ~0x00000001);
      family_ = getDefaultInstance().getFamily();
      onChanged();
      return this;
    }

    private com.google.protobuf.ByteString qualifier_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <code>bytes qualifier = 2;</code>
     * @return The qualifier.
     */
    @java.lang.Override
    public com.google.protobuf.ByteString getQualifier() {
      return qualifier_;
    }
    /**
     * <code>bytes qualifier = 2;</code>
     * @param value The qualifier to set.
     * @return This builder for chaining.
     */
    public Builder setQualifier(com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      qualifier_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <code>bytes qualifier = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearQualifier() {
      bitField0_ = (bitField0_ & ~0x00000002);
      qualifier_ = getDefaultInstance().getQualifier();
      onChanged();
      return this;
    }

    private com.google.protobuf.ByteString value_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <code>bytes value = 3;</code>
     * @return The value.
     */
    @java.lang.Override
    public com.google.protobuf.ByteString getValue() {
      return value_;
    }
    /**
     * <code>bytes value = 3;</code>
     * @param value The value to set.
     * @return This builder for chaining.
     */
    public Builder setValue(com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      value_ = value;
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <code>bytes value = 3;</code>
     * @return This builder for chaining.
     */
    public Builder clearValue() {
      bitField0_ = (bitField0_ & ~0x00000004);
      value_ = getDefaultInstance().getValue();
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:CellParam)
  }

  // @@protoc_insertion_point(class_scope:CellParam)
  private static final hbasecli.binary.model.CellParam DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new hbasecli.binary.model.CellParam();
  }

  public static hbasecli.binary.model.CellParam getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CellParam>
      PARSER = new com.google.protobuf.AbstractParser<CellParam>() {
    @java.lang.Override
    public CellParam parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<CellParam> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CellParam> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public hbasecli.binary.model.CellParam getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

