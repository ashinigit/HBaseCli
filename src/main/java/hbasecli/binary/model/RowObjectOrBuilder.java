// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Protos.proto

// Protobuf Java Version: 3.25.3
package hbasecli.binary.model;

public interface RowObjectOrBuilder extends
    // @@protoc_insertion_point(interface_extends:RowObject)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>bytes rowKey = 1;</code>
   * @return The rowKey.
   */
  com.google.protobuf.ByteString getRowKey();

  /**
   * <code>repeated .CellObject cells = 2;</code>
   */
  java.util.List<hbasecli.binary.model.CellObject> 
      getCellsList();
  /**
   * <code>repeated .CellObject cells = 2;</code>
   */
  hbasecli.binary.model.CellObject getCells(int index);
  /**
   * <code>repeated .CellObject cells = 2;</code>
   */
  int getCellsCount();
  /**
   * <code>repeated .CellObject cells = 2;</code>
   */
  java.util.List<? extends hbasecli.binary.model.CellObjectOrBuilder> 
      getCellsOrBuilderList();
  /**
   * <code>repeated .CellObject cells = 2;</code>
   */
  hbasecli.binary.model.CellObjectOrBuilder getCellsOrBuilder(
      int index);
}
