package hbasecli.model.bytes;

import java.util.Collection;

public class RowInfo {

    private byte[] rowKey;

    private Collection<CellInfo> cells;

    public byte[] getRowKey() {
        return rowKey;
    }

    public void setRowKey(byte[] rowKey) {
        this.rowKey = rowKey;
    }

    public Collection<CellInfo> getCells() {
        return cells;
    }

    public void setCells(Collection<CellInfo> cells) {
        this.cells = cells;
    }

}
