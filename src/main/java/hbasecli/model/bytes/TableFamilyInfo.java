package hbasecli.model.bytes;

import java.util.Map;

public class TableFamilyInfo {

    private byte[] name;

    private Map<byte[], byte[]> info;

    public byte[] getName() {
        return name;
    }

    public void setName(byte[] name) {
        this.name = name;
    }

    public Map<byte[],byte[]> getInfo() {
        return info;
    }

    public void setInfo(Map<byte[],byte[]> info) {
        this.info = info;
    }

}
