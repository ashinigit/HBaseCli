package hbasecli.model.bytes;

import java.util.Collection;

public class TableInfo {

    private String name;

    private Collection<TableFamilyInfo> families;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TableFamilyInfo> getFamilies() {
        return families;
    }

    public void setFamilies(Collection<TableFamilyInfo> families) {
        this.families = families;
    }

}