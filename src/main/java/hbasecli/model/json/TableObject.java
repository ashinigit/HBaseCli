package hbasecli.model.json;

import java.util.Collection;

public class TableObject {

    private String name;

    private Collection<TableFamilyObject> families;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TableFamilyObject> getFamilies() {
        return families;
    }

    public void setFamilies(Collection<TableFamilyObject> families) {
        this.families = families;
    }

}