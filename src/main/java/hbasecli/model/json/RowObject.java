package hbasecli.model.json;

import java.util.Collection;

public class RowObject {

    private String rowKey;

    private Collection<CellObject> cells;

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public Collection<CellObject> getCells() {
        return cells;
    }

    public void setCells(Collection<CellObject> cells) {
        this.cells = cells;
    }

}
