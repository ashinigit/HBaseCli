package hbasecli.model.json;

import java.util.Map;

public class TableFamilyObject {

    private String name;

    private Map<String, String> info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getInfo() {
        return info;
    }

    public void setInfo(Map<String, String> info) {
        this.info = info;
    }

}
