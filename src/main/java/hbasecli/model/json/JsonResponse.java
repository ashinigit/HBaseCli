package hbasecli.model.json;

public class JsonResponse<T> {

    private int code;

    private String message = "";

    private T data;

    public static <T> JsonResponse<T> of(int code, String message, T data) {
        JsonResponse<T> jsonResponse = new JsonResponse<>();
        jsonResponse.code = code;
        jsonResponse.message = message;
        jsonResponse.data = data;
        return jsonResponse;
    }

    public static <T> JsonResponse<T> failure(String message) {
        return of(1, message, null);
    }

    public static <T> JsonResponse<T> success(T data) {
        return of(0, "", data);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}