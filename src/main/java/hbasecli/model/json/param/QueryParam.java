package hbasecli.model.json.param;

import java.util.Map;

public class QueryParam {

    private String connectionId;

    private String tableName;

    private String rowKeyPattern;

    private String rowKeyDataType;

    private Map<String, String> valueDataTypeMap;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRowKeyPattern() {
        return rowKeyPattern;
    }

    public void setRowKeyPattern(String rowKeyPattern) {
        this.rowKeyPattern = rowKeyPattern;
    }

    public String getRowKeyDataType() {
        return rowKeyDataType;
    }

    public void setRowKeyDataType(String rowKeyDataType) {
        this.rowKeyDataType = rowKeyDataType;
    }

    public Map<String, String> getValueDataTypeMap() {
        return valueDataTypeMap;
    }

    public void setValueDataTypeMap(Map<String, String> valueDataTypeMap) {
        this.valueDataTypeMap = valueDataTypeMap;
    }

}