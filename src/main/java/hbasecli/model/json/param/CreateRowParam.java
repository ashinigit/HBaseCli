package hbasecli.model.json.param;

import java.util.Collection;

public class CreateRowParam {

    private String connectionId;

    private String tableName;

    private String rowKey;

    private String rowKeyDataType;

    private Collection<CellParam> cells;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public String getRowKeyDataType() {
        return rowKeyDataType;
    }

    public void setRowKeyDataType(String rowKeyDataType) {
        this.rowKeyDataType = rowKeyDataType;
    }

    public Collection<CellParam> getCells() {
        return cells;
    }

    public void setCells(Collection<CellParam> cells) {
        this.cells = cells;
    }

    public static class CellParam {

        private String family;

        private String qualifier;

        private String value;

        private String valueDataType;

        public String getFamily() {
            return family;
        }

        public void setFamily(String family) {
            this.family = family;
        }

        public String getQualifier() {
            return qualifier;
        }

        public void setQualifier(String qualifier) {
            this.qualifier = qualifier;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getValueDataType() {
            return valueDataType;
        }

        public void setValueDataType(String valueDataType) {
            this.valueDataType = valueDataType;
        }

    }

}
