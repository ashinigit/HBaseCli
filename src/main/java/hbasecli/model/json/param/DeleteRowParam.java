package hbasecli.model.json.param;

import java.util.List;

public class DeleteRowParam {

    private String connectionId;

    private String tableName;

    private List<String> rowKeys;

    private String rowKeyDataType;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getRowKeys() {
        return rowKeys;
    }

    public void setRowKeys(List<String> rowKeys) {
        this.rowKeys = rowKeys;
    }

    public String getRowKeyDataType() {
        return rowKeyDataType;
    }

    public void setRowKeyDataType(String rowKeyDataType) {
        this.rowKeyDataType = rowKeyDataType;
    }

}
