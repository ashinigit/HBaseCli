package hbasecli.model.json.param;

import java.util.ArrayList;
import java.util.List;

public class CreateTableParam {

    private String connectionId;

    private String tableName;

    private List<String> families = new ArrayList<>();

    private Boolean deleteIfExists = false;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getFamilies() {
        return families;
    }

    public void setFamilies(List<String> families) {
        this.families = families;
    }

    public Boolean getDeleteIfExists() {
        return deleteIfExists;
    }

    public void setDeleteIfExists(Boolean deleteIfExists) {
        this.deleteIfExists = deleteIfExists;
    }

}
