package hbasecli.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hbasecli.model.json.JsonResponse;
import hbasecli.model.json.RowObject;
import hbasecli.model.json.param.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RowControllerTest {

    @Resource
    private TestRestTemplate restTemplate;

    @Resource
    private ObjectMapper objectMapper;

    @Test
    public void query() throws JsonProcessingException {
        QueryParam param = new QueryParam();
        param.setConnectionId("connectionId");
        param.setTableName("tableName");
        param.setRowKeyPattern("rowKey");
        param.setRowKeyDataType("UTF-8");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/row/query",
                HttpMethod.POST,
                new HttpEntity<>(param, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                JsonResponse<List<RowObject>> response = objectMapper.readValue(
                        new String(bytes, StandardCharsets.UTF_8),
                        new TypeReference<JsonResponse<List<RowObject>>>() {}
                );
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getData());
            }
        }
    }

    private static CreateRowParam buildCreateRowParam() {
        CreateRowParam param = new CreateRowParam();
        param.setConnectionId("connectionId");
        param.setTableName("tableName");
        param.setRowKey("rowKey");
        param.setRowKeyDataType("UTF-8");
        param.setCells(new ArrayList<CreateRowParam.CellParam>() {{
            CreateRowParam.CellParam cellParam1 = new CreateRowParam.CellParam();
            cellParam1.setFamily("");
            cellParam1.setQualifier("");
            cellParam1.setValue("");
            cellParam1.setValueDataType("UTF-8");
            add(cellParam1);
            CreateRowParam.CellParam cellParam2 = new CreateRowParam.CellParam();
            cellParam2.setFamily("");
            cellParam2.setQualifier("");
            cellParam2.setValue("");
            cellParam2.setValueDataType("UTF-8");
            add(cellParam2);
            CreateRowParam.CellParam cellParam3 = new CreateRowParam.CellParam();
            cellParam3.setFamily("");
            cellParam3.setQualifier("");
            cellParam3.setValue("");
            cellParam3.setValueDataType("UTF-8");
            add(cellParam3);
        }});
        return param;
    }

    @Test
    public void createRow() throws JsonProcessingException {
        CreateRowParam param = buildCreateRowParam();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/row/create",
                HttpMethod.POST,
                new HttpEntity<>(param, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                JsonResponse<Boolean> response = objectMapper.readValue(
                        new String(bytes, StandardCharsets.UTF_8),
                        new TypeReference<JsonResponse<Boolean>>() {}
                );
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getData());
            }
        }
    }

    @Test
    public void deleteRow() throws JsonProcessingException {
        DeleteRowParam param = new DeleteRowParam();
        param.setConnectionId("connectionId");
        param.setTableName("tableName");
        param.setRowKeys(new ArrayList<String>() {{
            add("rowKey1");
            add("rowKey2");
            add("rowKey3");
        }});
        param.setRowKeyDataType("UTF-8");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/row/delete",
                HttpMethod.POST,
                new HttpEntity<>(param, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                JsonResponse<Boolean> response = objectMapper.readValue(
                        new String(bytes, StandardCharsets.UTF_8),
                        new TypeReference<JsonResponse<Boolean>>() {}
                );
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getData());
            }
        }
    }

}
