package hbasecli.binary.controller;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import hbasecli.binary.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BinaryControllerTest {

    @Resource
    private TestRestTemplate restTemplate;

    @Test
    public void query() throws InvalidProtocolBufferException {
        QueryParam param = QueryParam.newBuilder()
                .setConnectionId("connectionId")
                .setTableName("tableName")
                .setRowKeyPattern(ByteString.copyFrom(new byte[]{(byte) 0}))
                .build();
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", new ByteArrayResource(param.toByteArray()) {
            @Override
            public String getFilename() {
                return "test";
            }
        });
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/binary/row/query",
                HttpMethod.POST,
                new HttpEntity<>(form, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                RowObjectsResponse response = RowObjectsResponse.parseFrom(bytes);
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getDataList());
            }
        }
    }

    @Test
    public void createRow() throws InvalidProtocolBufferException {
        CreateRowParam param = CreateRowParam.newBuilder()
                .setConnectionId("connectionId")
                .setTableName("tableName")
                .setRowKey(ByteString.copyFrom(new byte[]{(byte) 0}))
                .addCells(CellParam.newBuilder()
                        .setFamily(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setQualifier(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setValue(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .build())
                .addCells(CellParam.newBuilder()
                        .setFamily(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setQualifier(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setValue(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .build())
                .addCells(CellParam.newBuilder()
                        .setFamily(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setQualifier(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .setValue(ByteString.copyFrom(new byte[]{(byte) 0}))
                        .build())
                .build();
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", new ByteArrayResource(param.toByteArray()) {
            @Override
            public String getFilename() {
                return "test";
            }
        });
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/binary/row/create",
                HttpMethod.POST,
                new HttpEntity<>(form, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                BooleanResponse response = BooleanResponse.parseFrom(bytes);
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getData());
            }
        }
    }

    @Test
    public void deleteRow() throws InvalidProtocolBufferException {
        DeleteRowParam param = DeleteRowParam.newBuilder()
                .setConnectionId("connectionId")
                .setTableName("tableName")
                .addRowKeys(ByteString.copyFrom(new byte[]{(byte) 0}))
                .addRowKeys(ByteString.copyFrom(new byte[]{(byte) 0}))
                .addRowKeys(ByteString.copyFrom(new byte[]{(byte) 0}))
                .build();
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", new ByteArrayResource(param.toByteArray()) {
            @Override
            public String getFilename() {
                return "test";
            }
        });
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(
                "/api/binary/row/delete",
                HttpMethod.POST,
                new HttpEntity<>(form, httpHeaders),
                byte[].class
        );
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            byte[] bytes = responseEntity.getBody();
            if (bytes != null) {
                System.out.println("size = " + bytes.length + " Byte");
                BooleanResponse response = BooleanResponse.parseFrom(bytes);
                System.out.println("code = " + response.getCode());
                System.out.println("message = " + response.getMessage());
                System.out.println("data = " + response.getData());
            }
        }
    }

}
